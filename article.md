title: "Vejce a jablka dražší i o třetinu. Podívejte se, jak se mění ceny základních potravin"
perex: ""
authors: ["Michal Zlatkovský"]
published: "22. června 2017"
coverimg: https://www.irozhlas.cz/sites/default/files/uploader/jablka_170622-002347_zlo.jpg
coverimg_note: "Foto <a href='https://commons.wikimedia.org/wiki/Apples#/media/File:Assorted_Red_and_Green_Apples_2120px.jpg'>Wikimedia Commons/NaJina McEnany (CC BY-SA 2.5)</a>"
styles: []
libraries: ["https://unpkg.com/jquery@3.2.1","https://code.highcharts.com/highcharts.src.js"]
options: "" #wide
---

Ceny dvanácti potravin ve spotřebním koši se v posledních dvou letech změnily až o desítky procent. Kromě másla podražila i jablka nebo vejce, zatímco třeba kuřecí maso oproti roku 2015 zlevnilo. Vyplývá to z přehledu běžných - tedy neakčních - cen potravin poskytovaných nákupními řetězci.

Za kolísajícími cenami stojí zejména u ovoce a zeleniny sezónní trendy i změny na světovém trhu. V posledních týdnech se v Česku mluvilo zejména o podražení másla způsobeném snížením jeho celosvětové produkce. Drahým máslem [se zabývá](https://zpravy.aktualne.cz/finance/nakupovani/maslo-stoji-o-deset-korun-vice-nez-pred-rokem-drahe-zustane/r~0aff68ca503911e7898e002590604f2e/) i ministerstvo zemědělství.

<wide><div id="graf_ovozel"></div></wide>

Až na výjimky se ceny drží v poměrně stabilní hladině. Dlouhodobě zlevnilo například kuřecí maso - kilogram kuřete se do září 2015 držel na ceně kolem 85 korun, na konci předloňského roku však jeho cena klesla a dodnes se drží pod 75 korunami.

<wide><div id="graf_maso"></div></wide>

Balení deseti vajec se mezi lety 2015 a 2016 drželo pod 29 korunami, loni v listopadu však opět překročilo třicetikorunovou hranici a dnes se drží kolem 33 korun. Jednou z nejspolehlivějších komodit oproti tomu zůstává Plzeňský prazdroj, jehož půllitrová láhev se za poslední dva roky příliš nevychýlila od ceny 25 korun.

<wide><div id="graf_ostatni"></div></wide>

Data získal server iRozhlas.cz od databáze letáků [Kupi](https://www.kupi.cz/). Liší se od [pravidelných šetření](https://www.czso.cz/csu/czso/setreni-prumernych-cen-vybranych-vyrobku-potravinarske-vyrobky-casove-rady) Českého statistického úřadu, který na základě vzorku vybraných prodejen vypočítává takzvaný index spotřebitelských cen. Letáky oproti tomu poskytují především větší obchody a řetězce, proto se průměry cen mírně liší, navíc se do nic nezapočítává zboží v akci. Trendy ale oba zdroje dat ukazují shodné.

Kromě průměrů běžných cen základních potravin se můžete podívat i na částky z akčních letáků. Jednotlivé potraviny vybírejte stisknutím jejich názvů pod grafem.

<wide><div id="graf_akce"></div></wide>